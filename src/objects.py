from trainerbase.gameobject import GameFloat, GameInt
from trainerbase.memory import Address, pm

from memory import last_used_item_count_pointer


base_address = Address(pm.base_address + 0x1ADC9C8, [0x0, 0x10, 0x38, 0x50, 0x10])
player_base_address = base_address.inherit(extra_offsets=[0x9C0])

strength = GameInt(player_base_address.inherit(new_add=0x24))
constitution = GameInt(player_base_address.inherit(new_add=0x28))
dexterity = GameInt(player_base_address.inherit(new_add=0x2C))
intelligence = GameInt(player_base_address.inherit(new_add=0x30))
cunning = GameInt(player_base_address.inherit(new_add=0x34))
cold = GameInt(player_base_address.inherit(new_add=0x3C))

jetpack_fuel = GameFloat(Address(pm.base_address + 0x1B0F7E8, [0x178]))
xp = GameInt(player_base_address.inherit(new_add=0x38))
lp = GameInt(player_base_address.inherit(new_add=0x40))
ap = GameInt(player_base_address.inherit(new_add=0x44))
level = GameInt(player_base_address.inherit(new_add=0x25C))

max_hp = GameInt(player_base_address.inherit(new_add=0x240))
hp = GameInt(player_base_address.inherit(new_add=0x244))
max_mana = GameInt(player_base_address.inherit(new_add=0x248))
mana = GameInt(player_base_address.inherit(new_add=0x24C))
max_stamina = GameInt(player_base_address.inherit(new_add=0x258))
stamina = GameFloat(base_address.inherit(extra_offsets=[0x9C0 + 0x940, 0x108]))
max_elex = GameInt(player_base_address.inherit(new_add=0x250))
elex = GameInt(player_base_address.inherit(new_add=0x254))

last_used_item_count = GameInt(Address(last_used_item_count_pointer, [0x3C]))
