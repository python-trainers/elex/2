from os import _exit as force_exit

from trainerbase.codeinjection import safely_eject_all_code_injections
from trainerbase.scriptengine import system_script_engine

from gui import run_menu
from injections import update_last_used_item_count_pointer
from scripts import regeneration_script_engine


def on_initialized():
    system_script_engine.start()
    regeneration_script_engine.start()

    update_last_used_item_count_pointer.inject()


def on_shutdown():
    system_script_engine.stop()
    regeneration_script_engine.stop()

    safely_eject_all_code_injections()


def main():
    run_menu(on_initialized=on_initialized)
    on_shutdown()
    force_exit(0)


if __name__ == "__main__":
    main()
