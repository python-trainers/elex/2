from trainerbase.common import regenerate
from trainerbase.scriptengine import ScriptEngine

from objects import hp, mana, max_hp, max_mana, elex, max_elex


regeneration_script_engine = ScriptEngine(delay=0.7)


@regeneration_script_engine.simple_script
def hp_regeneration():
    regenerate(hp, max_hp, percent=1, min_value=2)


@regeneration_script_engine.simple_script
def mana_regeneration():
    regenerate(mana, max_mana, percent=2, min_value=2)


@regeneration_script_engine.simple_script
def elex_regeneration():
    regenerate(elex, max_elex, percent=2, min_value=2)
