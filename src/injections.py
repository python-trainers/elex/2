from trainerbase.codeinjection import AllocatingCodeInjection, CodeInjection
from trainerbase.memory import pm

from memory import last_used_item_count_pointer


inf_ammo = CodeInjection(pm.base_address + 0x7BA0DC, b"\xEB")
no_reload = CodeInjection(pm.base_address + 0x8C2936, "nop\nnop")


update_last_used_item_count_pointer = AllocatingCodeInjection(
    pm.base_address + 0x5B474F,
    f"""
        pop rax

        mov eax, ebx

        mov rsi, {last_used_item_count_pointer}
        mov [rsi], rcx

        mov rsi, rcx
        cmovg eax, r9d
        mov r14, r8
        mov [rcx + 0x3C], eax

        push rax
    """,
    original_code_length=15,
    is_long_x64_jump_needed=True,
)
