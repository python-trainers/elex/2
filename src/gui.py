from dearpygui import dearpygui as dpg
from trainerbase.gui import add_codeinjection_to_gui, add_gameobject_to_gui, add_script_to_gui, simple_trainerbase_menu

from injections import inf_ammo, no_reload
from objects import (
    ap,
    cold,
    constitution,
    cunning,
    dexterity,
    elex,
    hp,
    intelligence,
    jetpack_fuel,
    last_used_item_count,
    level,
    lp,
    mana,
    max_elex,
    max_hp,
    max_mana,
    max_stamina,
    stamina,
    strength,
    xp,
)
from scripts import hp_regeneration, mana_regeneration, elex_regeneration


@simple_trainerbase_menu("Elex II", 700, 400)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Stats"):
            add_gameobject_to_gui(max_hp, "Max HP")
            add_gameobject_to_gui(hp, "HP")
            add_gameobject_to_gui(max_mana, "Max Mana")
            add_gameobject_to_gui(mana, "Mana")
            add_gameobject_to_gui(max_stamina, "Max Stamina")
            add_gameobject_to_gui(stamina, "Stamina", "F1")
            add_gameobject_to_gui(max_elex, "Max Elex")
            add_gameobject_to_gui(elex, "Elex")

        with dpg.tab(label="Attributes"):
            add_gameobject_to_gui(strength, "Strength")
            add_gameobject_to_gui(constitution, "Constitution")
            add_gameobject_to_gui(dexterity, "Dexterity")
            add_gameobject_to_gui(intelligence, "Intelligence")
            add_gameobject_to_gui(cunning, "Cunning")
            add_gameobject_to_gui(cold, "Cold")

        with dpg.tab(label="Level Related"):
            add_gameobject_to_gui(xp, "XP")
            add_gameobject_to_gui(lp, "LP")
            add_gameobject_to_gui(ap, "AP")
            add_gameobject_to_gui(level, "Level")

        with dpg.tab(label="Scripts & Injections"):
            add_script_to_gui(hp_regeneration, "HP Regeneration", "F2")
            add_script_to_gui(mana_regeneration, "Mana Regeneration", "F3")
            add_script_to_gui(elex_regeneration, "Elex Regeneration", "F4")
            add_codeinjection_to_gui(inf_ammo, "Infinite Ammo", "F6")
            add_codeinjection_to_gui(no_reload, "No Reload", "F7")

        with dpg.tab(label="Misc"):
            add_gameobject_to_gui(jetpack_fuel, "Jetpack Fuel", "F8")
            add_gameobject_to_gui(last_used_item_count, "Last Used Item Count")
